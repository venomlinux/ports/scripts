#!/bin/sh

as_root()
{
	if [ $(id -u) = 0 ]; then
		$*
	elif [ -x /usr/bin/sudo ]; then
		sudo $*
	elif [ -x /usr/bin/doas ]; then
		doas $*
	else
		su -c \\"$*\\"
	fi
}

s6repos() {
mkdir -p $ROOTFS/etc/
cat > $ROOTFS/etc/scratchpkg.repo.spkgnew << "EOF"
#
# /etc/scratchpkg.repo : scratchpkg repo file
#
# format:
#    <repo directory> <repo url> <repo branch, "main" by default>
#
/usr/ports/main       https://codeberg.org/venomlinux/main
/usr/ports/community  https://codeberg.org/venomlinux/community
/usr/ports/s6         https://codeberg.org/venomlinux/s6
#/usr/ports/kde-lxqt   https://codeberg.org/venomlinux/kde-lxqt
#/usr/ports/mate       https://codeberg.org/venomlinux/mate
#/usr/ports/multilib   https://codeberg.org/venomlinux/multilib
#/usr/ports/nonfree    https://codeberg.org/venomlinux/nonfree
#/usr/ports/testing    https://codeberg.org/venomlinux/testing
#/usr/ports/xfce       https://codeberg.org/venomlinux/xfce
EOF
}

cleandirs() {

	[ -d $ROOTFS/etc/rc.d ] && as_root rm -fr $ROOTFS/etc/rc.d

}

PORTSDIR="$(dirname $(dirname $(realpath $0)))"
SCRIPTDIR="$(dirname $(realpath $0))"
ROOTFS="${ROOTFS:-$PORTSDIR/rootfs}"

[ -f $SCRIPTDIR/config ] && . $SCRIPTDIR/config

as_root $SCRIPTDIR/builder.sh -s6 -zap -rebase || exit 1

s6repos

cleandirs

as_root $SCRIPTDIR/builder.sh -s6 -rootfs || exit 1

exit 0
