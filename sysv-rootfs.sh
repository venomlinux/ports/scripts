#!/bin/sh

as_root()
{
	if [ $(id -u) = 0 ]; then
		$*
	elif [ -x /usr/bin/sudo ]; then
		sudo $*
	elif [ -x /usr/bin/doas ]; then
		doas $*
	else
		su -c \\"$*\\"
	fi
}


PORTSDIR="$(dirname $(dirname $(realpath $0)))"
SCRIPTDIR="$(dirname $(realpath $0))"

[ -f $SCRIPTDIR/config ] && . $SCRIPTDIR/config

as_root $SCRIPTDIR/builder.sh -zap -rebase -rootfs || exit 1

exit 0
