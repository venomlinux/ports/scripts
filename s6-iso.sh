#!/bin/sh

# The goal is add options to choose init and iso type ( xorg, wayland, base)

msgerr() {                                                                                   
	echo "!> $*"
}

die() {
	[ "$@" ] && msgerr $@
	exit 1
}

as_root()
{
	if [ $(id -u) = 0 ]; then
		$*
	elif [ -x /usr/bin/sudo ]; then
		sudo $*
	elif [ -x /usr/bin/doas ]; then
		doas $*
	else
		su -c \\"$*\\"
	fi
}


base() {
MUST_PKG="base-s6 dhcpcd-s6 wpa_supplicant os-prober grub"
MAIN_PKG="sudo alsa-utils dosfstools mtools htop fzy"
pkgs="$(echo $MUST_PKG $MAIN_PKG | tr ' ' ',')"
outputiso="$PORTSDIR/venomlinux-base-$INIT-$(uname -m)-$(date +%Y%m%d).iso"
}

xorg() {
MUST_PKG="base-s6 dbus-s6 dhcpcd-s6 networkmanager-s6 wpa_supplicant-s6 os-prober grub"  # must have pkg in the iso
XORG_PKG="xorg xorg-video-drivers xf86-input-libinput libmd libxkbcommon"
MAIN_PKG="sudo alsa-utils gparted dosfstools mtools gvfs networkmanager fastfetch xdg-user-dirs polkit-gnome ffmpeg geany firefox slim slim-themes fzy"
OPENBOX_PKG="openbox lxappearance lxappearance-obconf obconf obmenu-generator gmrun pcmanfm leafpad feh tint2 consolekit2 irssi mc htop"
THEME_PKG="arcbox papirus-icon-theme-dark papirus-icon-theme-light osx-arc-theme ttf-liberation picom dunst dfc"
pkgs="$(echo $MUST_PKG $XORG_PKG $MAIN_PKG $OPENBOX_PKG $THEME_PKG | tr ' ' ',')"
outputiso="$PORTSDIR/venomlinux-$INIT-$(uname -m)-$(date +%Y%m%d).iso"
}

wayland() {
MUST_PKG="base-s6 dhcpcd-s6 networkmanager-s6 wpa_supplicant-s6 os-prober grub"
WAYLAND_PKG="xcb-util-cursor xcb-util-keysyms libxfont2 libxcvt libtirpc xwayland"
MAIN_PKG="sudo alsa-utils dosfstools mtools gvfs fastfetch firefox nnn irssi htop wireplumber pipewire fzy pcmanfm upower"
SWAY_PKG="nwg-shell"
THEME_PKG="osx-arc-theme ttf-mononoki"
pkgs="$(echo $MUST_PKG $WAYLAND_PKG $MAIN_PKG $SWAY_PKG $THEME_PKG | tr ' ' ',')"
outputiso="$PORTSDIR/venomlinux-wayland-$INIT-$(uname -m)-$(date +%Y%m%d).iso"
}


s6repos() {
mkdir -p $ROOTFS/etc/
cat > $ROOTFS/etc/scratchpkg.repo.spkgnew << "EOF"
#
# /etc/scratchpkg.repo : scratchpkg repo file
#
# format:
#    <repo directory> <repo url> <repo branch, "main" by default>
#

/usr/ports/main         https://gitlab.com/venomlinux/ports/main
/usr/ports/community	https://gitlab.com/venomlinux/ports/community
#/usr/ports/kde-lxqt	https://gitlab.com/venomlinux/ports/kde-lxqt
#/usr/ports/mate	https://gitlab.com/venomlinux/ports/mate
#/usr/ports/multilib	https://gitlab.com/venomlinux/ports/multilib
#/usr/ports/nonfree     https://gitlab.com/venomlinux/ports/nonfree
/usr/ports/s6           https://gitlab.com/venomlinux/ports/s6
#/usr/ports/testing     https://gitlab.com/venomlinux/ports/testing
#/usr/ports/xfce        https://gitlab.com/venomlinux/ports/xfce
EOF
}

RELEASE=rolling
PORTSDIR="$(dirname $(dirname $(realpath $0)))"
SCRIPTDIR="$(dirname $(realpath $0))"
ROOTFS="$PWD/rootfs"
REPO="s6 main"
INIT="s6"

[ -f $SCRIPTDIR/config ] && . $SCRIPTDIR/config

case "$1" in
    -x) xorg 
	TP="xorg" ;;
    -w) wayland 
	TP="wayland" ;;
    -b) base 
	TP="base" ;;
     *) die "You need to choose a type of iso [-b(ase) -x(org) -w(ayland)]" ;;
esac

as_root $SCRIPTDIR/builder.sh \
		-s6 \
		-zap \
		-rebase \
		-pkg="$pkgs"|| exit 1 

s6repos

# mounts to foreground
sed '/{ mount -o remount,rw \/ }/s/\bif\b/foreground/' -i $ROOTFS/etc/s6/sv/remount-root/up
sed 's/if {/foreground {/g' -i $ROOTFS/etc/s6/sv/mount-*/up
#awk 'BEGIN {print "foreground {"} {print} END {print "}"}' $ROOTFS/etc/s6/sv/mount-filesystems/up > $ROOTFS/tmp/.upmfs && \
#	mv -f $ROOTFS/tmp/.upmfs $ROOTFS/etc/s6/sv/mount-filesystems/up
awk 'BEGIN {print "foreground {"} {print} END {print "}"}' $ROOTFS/etc/s6/sv/mount-procfs/up > $ROOTFS/tmp/.upmpfs && \
	mv -f $ROOTFS/tmp/.upmpfs $ROOTFS/etc/s6/sv/mount-procfs/up

case $TP in
	base)	
	    as_root $ROOTFS/usr/bin/xchroot $ROOTFS s6-service add default dhcpcd
	    as_root $ROOTFS/usr/bin/xchroot $ROOTFS s6-db-reload -r ;;
	wayland|xorg)
	    as_root $ROOTFS/usr/bin/xchroot $ROOTFS s6-service add default dbus NetworkManager
	    as_root $ROOTFS/usr/bin/xchroot $ROOTFS s6-db-reload -r ;;
esac


as_root $SCRIPTDIR/builder.sh \
	-iso \
	-outputiso="$outputiso" || exit 1

exit 0
