# scripts

This repository contains scripts used for maintaining Venom Linux, including release engineering.
All scripts are written in posix sh and most of them have names describing their function.

## builder.sh
`builder.sh` is the most complex script of the repository. It is used for operations on a rootfs, such as building packages and creating updated rootfs tarbals and isos.
With `./buider.sh -h` one can see its option and basic usage. It has minimal requirements and if called with an argument that will require something not available in the system it will inform the user.

It is used by other wrapper scripts in the repo to accomplish specific tasks:
- `s6-rootfs`: Creates an updated rootfs tarball with s6 init system.
- `s6-iso`: Creates a base, xorg and wayland iso with s6 init systems. Arguments: [-b(ase) -x(org) -x(ayland)]
- `sysv-rootfs`: Creates an updated rootfs tarball with SysVinit system.
- `sysv-iso`: Creates a base, xorg and wayland iso with SysVinit systems. Arguments: [-b(ase) -x(org) -x(ayland)]
- `portupdate.sh`: Updates ports in a chroot.

