#!/bin/sh

# The goal is add options to choose init and iso type ( xorg, wayland, base)

msgerr() {                                                                                   
	echo "!> $*"
}

die() {
	[ "$@" ] && msgerr $@
	exit 1
}

as_root()
{
	if [ $(id -u) = 0 ]; then
		$*
	elif [ -x /usr/bin/sudo ]; then
		sudo $*
	elif [ -x /usr/bin/doas ]; then
		doas $*
	else
		su -c \\"$*\\"
	fi
}


base() {
MUST_PKG="wpa_supplicant os-prober grub"
MAIN_PKG="sudo alsa-utils dosfstools mtools htop fzy"

outputiso="$PORTSDIR/venomlinux-base-$INIT-$(uname -m)-$(date +%Y%m%d).iso"
pkgs="$(echo $MUST_PKG $MAIN_PKG | tr ' ' ',')"
}

xorg() {
MUST_PKG="wpa_supplicant os-prober grub zstd"
XORG_PKG="xorg xorg-video-drivers xf86-input-libinput libmd libxkbcommon"
MAIN_PKG="sudo alsa-utils gparted dosfstools mtools gvfs networkmanager fastfetch xdg-user-dirs polkit-gnome ffmpeg geany firefox fzy"
OPENBOX_PKG="openbox lxappearance lxappearance-obconf obconf obmenu-generator gmrun pcmanfm leafpad feh tint2 consolekit2 irssi mc htop"
THEME_PKG="arcbox papirus-icon-theme-dark papirus-icon-theme-light osx-arc-theme ttf-liberation picom dunst dfc"

outputiso="$PORTSDIR/venomlinux-xorg-$INIT-$(uname -m)-$(date +%Y%m%d).iso"
pkgs="$(echo $MUST_PKG $XORG_PKG $MAIN_PKG $OPENBOX_PKG $THEME_PKG | tr ' ' ',')"
}

wayland() {
MUST_PKG="wpa_supplicant os-prober grub"
WAYLAND_PKG="xcb-util-cursor xcb-util-keysyms libxfont2 libxcvt libtirpc xwayland"
MAIN_PKG="sudo alsa-utils dosfstools mtools gvfs fastfetch firefox nnn irssi htop wireplumber pipewire fzy pcmanfm upower"
SWAY_PKG="nwg-shell"
THEME_PKG="osx-arc-theme ttf-mononoki"

outputiso="$PORTSDIR/venomlinux-wayland-$INIT-$(uname -m)-$(date +%Y%m%d).iso"
pkgs="$(echo $MUST_PKG $WAYLAND_PKG $MAIN_PKG $SWAY_PKG $THEME_PKG | tr ' ' ',')"
}


RELEASE=rolling
PORTSDIR="$(dirname $(dirname $(realpath $0)))"
SCRIPTDIR="$(dirname $(realpath $0))"
ROOTFS="$PWD/rootfs"

INIT="$2"
INIT=runit

[ -f $SCRIPTDIR/config ] && . $SCRIPTDIR/config

case "$1" in
    -x) xorg ;; 
    -w) wayland ;;
    -b) base ;;
     *) die "You need to choose a type of iso [-b(ase) -x(org) -w(ayland)]" ;;
esac

as_root $SCRIPTDIR/builder.sh \
	-runit \
	-zap \
	-rebase \
	-iso \
	-outputiso="$outputiso" \
	-pkg="$pkgs" || exit 1

exit 0

