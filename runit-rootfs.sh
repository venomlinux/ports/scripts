#!/bin/sh

as_root()
{
	if [ $(id -u) = 0 ]; then
		$*
	elif [ -x /usr/bin/sudo ]; then
		sudo $*
	elif [ -x /usr/bin/doas ]; then
		doas $*
	else
		su -c \\"$*\\"
	fi
}

cleandirs() {

	[ -d $ROOTFS/etc/rc.d ] && as_root rm -fr $ROOTFS/etc/rc.d

}

PORTSDIR="$(dirname $(dirname $(realpath $0)))"
SCRIPTDIR="$(dirname $(realpath $0))"
ROOTFS="${ROOTFS:-$PORTSDIR/rootfs}"

[ -f $SCRIPTDIR/config ] && . $SCRIPTDIR/config

as_root $SCRIPTDIR/builder.sh -runit -zap -rebase  || exit 1

cleandirs

as_root $SCRIPTDIR/builder.sh -runit -rootfs || exit 1


exit 0
